# Comments-on-HTML
Comments on HTML
<!-- start of introduction -->
 <h1>Current Exhibitions</h1>
 <h2>Olafur Eliasson</h2>
 <!-- end of introduction -->
 <!-- start of main text -->
 <p>Olafur Eliasson was born in Copenhagen, Denmark
      in 1967 to Icelandic parents.</p>
  <p>He is known for sculptures and larg-scale
	  installation art employing elemental materials
          such as light, water, and air temprature to enhance viewer's experience.</p>
  <!-- end of main text -->
  <!--
      <a href="mailto:info@example.org">Contact</a>
  -->
